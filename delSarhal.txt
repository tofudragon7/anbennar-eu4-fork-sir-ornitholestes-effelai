every_province = {
	limit = { continent = africa }
	add_base_tax = -20
	add_base_production = -20
	add_base_manpower = -20
	destroy_province = yes
}