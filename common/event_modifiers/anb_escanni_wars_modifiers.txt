
escanni_wars_escanni_imperialism = {
	province_warscore_cost = -0.15
	core_creation = -0.15
}

escanni_wars_escanni_peace = {
	development_cost = -0.15
	global_tax_modifier = 0.15
}

escanni_wars_escanni_emperor = {
	imperial_authority_value = 1
}
