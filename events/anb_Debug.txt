namespace = anb_debug

##Cleaning province
province_event = {
	id = anb_debug.3
	title = anb_debug.3.t
	desc = anb_debug.3.d
	picture = HUIZTILOPOCHTLI_eventPicture
	
	hidden = yes
	is_triggered_only = yes
	
	trigger = {
		has_province_flag = cant_colonize
	}
	
	option = {
		name = debug.6.a
		
		if = {
			limit = { 
				OR = { owner = { ai = no } is_city = yes }
			}
			clr_province_flag = cant_colonize
		}
		else = {
			add_colonysize = -1000
			clr_province_flag = cant_colonize
		}
	}
}

country_event = {
	id = anb_debug.100
	title = anb_debug.100.t
	desc = anb_debug.100.d
	picture = HUIZTILOPOCHTLI_eventPicture

	is_triggered_only = yes

	immediate = {
		artifice_breakdown = yes
		hidden_effect = {
			set_up_artifice_system = yes
			update_artifice_points = yes
		}
	}
	
	option = {
		name = debug.100.a
	}
}

country_event = {
	id = anb_debug.101
	title = anb_debug.100.t
	desc = anb_debug.100.d
	picture = HUIZTILOPOCHTLI_eventPicture

	is_triggered_only = yes

	immediate = {
		update_artifice_points = yes
	}
	
	option = {
		name = debug.100.a
	}
}
# namespace = anb_debug

# #Give Me Witch Kings
# country_event = {
	# id = anb_debug.1
	# title = anb_debug.1.t
	# desc = anb_debug.1.d
	# picture = HUIZTILOPOCHTLI_eventPicture
	
	# is_triggered_only = yes
	
	# # Come forth!
	# option = {
		# name = anb_debug.1.a
		# ai_chance = {
			# factor = 90
		# }
		
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		
	# }
# }

##Colonist stuck on settlement growth fix
#This is Heho code, we tried to change this. It didn't work. Lesson learned, don't touch Heho code.
# country_event = {
	# id = anb_debug.2
	# title = anb_debug.2.t
	# desc = anb_debug.2.d
	# picture = HUIZTILOPOCHTLI_eventPicture
	
	# hidden = yes
	# is_triggered_only = yes
	
	# trigger = {
		# has_province_flag = cant_colonize
	# }
	
	# option = {
		# name = debug.6.a
		
		# if = {
			# limit = { 
				# owner = { ai = no }
			# }
			# clr_province_flag = cant_colonize
		# }
		# else = {
			# add_colonysize = -1000
			# clr_province_flag = cant_colonize
		# }
	# }
# }

# # Testing
# country_event = {
	# id = anb_debug.9
	# title = anb_debug.3.t
	# desc = anb_debug.3.d
	# picture = HUIZTILOPOCHTLI_eventPicture
	
	# is_triggered_only = yes
	
	# option = {
		# name = debug.6.a
		
		# random_list = {
			# 1 = {
				# add_treasury = 1
			# }
			# 1 = {
				# modifier = {
					# factor = 2
					# treasury = 100
				# }
				# add_treasury = 10
			# }
			# 1 = {
				# trigger = {
					# treasury = 100
				# }
				# add_treasury = 100
			# }
		# }
	# }
# }
# namespace = anb_debug

# #Give Me Witch Kings
# country_event = {
	# id = anb_debug.1
	# title = anb_debug.1.t
	# desc = anb_debug.1.d
	# picture = HUIZTILOPOCHTLI_eventPicture
	
	# is_triggered_only = yes
	
	# # Come forth!
	# option = {
		# name = anb_debug.1.a
		# ai_chance = {
			# factor = 90
		# }
		
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		# increase_witch_king_points_large = yes
		
	# }
# }

##Colonist stuck on settlement growth fix
#This is Heho code, we tried to change this. It didn't work. Lesson learned, don't touch Heho code.
#country_event = {
	# id = anb_debug.2
	# title = anb_debug.2.t
	# desc = anb_debug.2.d
	# picture = HUIZTILOPOCHTLI_eventPicture
	
	# hidden = yes
	# is_triggered_only = yes
	
	# trigger = {
		# has_province_flag = cant_colonize
	# }
	
	# option = {
		# name = debug.6.a
		
		# if = {
			# limit = { 
				# owner = { ai = no }
			# }
			# clr_province_flag = cant_colonize
		# }
		# else = {
			# add_colonysize = -1000
			# clr_province_flag = cant_colonize
		# }
	# }
# }

# # Testing
# country_event = {
	# id = anb_debug.9
	# title = anb_debug.3.t
	# desc = anb_debug.3.d
	# picture = HUIZTILOPOCHTLI_eventPicture
	
	# is_triggered_only = yes
	
	# option = {
		# name = debug.6.a
		
		# random_list = {
			# 1 = {
				# add_treasury = 1
			# }
			# 1 = {
				# modifier = {
					# factor = 2
					# treasury = 100
				# }
				# add_treasury = 10
			# }
			# 1 = {
				# trigger = {
					# treasury = 100
				# }
				# add_treasury = 100
			# }
		# }
	# }
# }